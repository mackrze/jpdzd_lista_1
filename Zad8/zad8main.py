import sys
import os

if __name__ == '__main__':
    try:
        try:
            dir_path = os.path.dirname(os.path.realpath(__file__))
            text = os.path.join(dir_path, sys.argv[1])  # for problem with finding file only by name
            f = open(text, "a")
        except FileNotFoundError:
            f = open(text, "w")
        list_of_products = []
        while 1:
            product = input('Enter your product or type q to end:')
            if product == "q":  # if q then end
                break
            elif not product.isdigit() and not product[-1].isdigit():  # if user doesn't enter quantity
                product_quantity = input('Enter your product quantity:')
                if product_quantity.isdigit():
                    list_of_products.append(product + '\t' + product_quantity)
                    print("Product added to list")
                else:
                    pass
            elif not product.isdigit() and product[-1].isdigit():  # if user enter product and quantity
                product_as_list = product.split()
                product = product.replace(product[-(len(product_as_list[-1])):], "")
                list_of_products.append(product + '\t' + product_as_list[-1])
                print("Product added to list")
        for products in list_of_products:
            f.write(products + '\n')
        f.close()

    except IndexError:
        sys.stdout.write('Please enter proper amount of arguments')
        sys.exit(1)
