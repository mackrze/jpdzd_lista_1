import sys

if __name__ == '__main__':
    try:
        login = input("Your login:")
        if len(login) < 4:
            sys.stdout.write('Login must be longer than 3 characters')
            sys.exit(1)
        if login[0].isdigit():
            sys.stdout.write('Login must not start by a digit')
            sys.exit(1)
        for i in range(len(login)):
            if not(login[i].isdigit() or login[i].isalpha()):
                sys.stdout.write('Login must contain only letters and digits')
                sys.exit(1)

        sys.stdout.write('Login is valid')
    except IndexError:
        sys.stdout.write('Please enter proper amount of arguments')
        sys.exit(1)
