import sys


def celsjusz_fahrenheit(temp_c):
    return (temp_c * 9 / 5) + 32


def fahrenheit_celsjusz(temp_f):
    return (temp_f - 32) * 5 / 9


if __name__ == '__main__':
    try:
        conversion = sys.argv[1]
        temperature = float(sys.argv[2])
        if not(conversion == "Celsjusz-Fahrenheit" or conversion == "Fahrenheit-Celsjusz"):
            sys.stdout.write('conversion must be: Celsjusz-Fahrenheit or Fahrenheit-Celsjusz')
            sys.exit(1)
        if conversion == 'Celsjusz-Fahrenheit':
            temperature_new = round(celsjusz_fahrenheit(temperature), 1)
        else:
            temperature_new = round(fahrenheit_celsjusz(temperature), 1)

        sys.stdout.write('Input temperature: {} , new temperature: {}'.format(temperature, temperature_new))
    except ValueError:
        sys.stdout.write('Second argument must be temperature ')
    except IndexError:
        sys.stdout.write('Please enter proper amount of arguments')
        sys.exit(1)
