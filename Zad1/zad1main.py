import math
import sys

if __name__ == '__main__':

    try:
        x1 = int(sys.argv[1])
        y1 = int(sys.argv[2])
        x2 = int(sys.argv[3])
        y2 = int(sys.argv[4])
        print('Input OK: Starting program')
    except ValueError:
        sys.stdout.write('Please enter a proper number')
        sys.exit(1)
    except IndexError:
        sys.stdout.write('Please enter proper amount of arguments')
        sys.exit(1)
    distance_power_2: float = pow(x2 - x1, 2) + pow(y2 - y1, 2)
    distance: float = round(math.sqrt(distance_power_2), 2)  # round: <0.5 => 0.0 else 1.0
    sys.stdout.write('Distance between points is: {}'.format(distance))
