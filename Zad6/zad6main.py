import sys
import os

if __name__ == '__main__':
    try:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text = os.path.join(dir_path, sys.argv[1])  # for problem with finding file by name
        f = open(text)
        lines = 0
        words = 0
        characters = 0
        for line in f:
            line = line.strip("\n")  # split whole text by new line without spaces
            words_in_line = line.split()  # split line by words
            lines += 1  # add one to lines
            words += len(words_in_line)  # add length of array with words in line
            for word in words_in_line:
                characters += len(word)  # add number of letters in every word in array of words form line (no spaces)

        f.close()
        print("Number of lines: {}, words: {}, characters: {}".format(lines, words, characters))

    except FileNotFoundError:
        sys.stdout.write('File not exist')
    except IndexError:
        sys.stdout.write('Please enter proper amount of arguments')
        sys.exit(1)
