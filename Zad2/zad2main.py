import sys

fibonacciElements = []

## początkowy indeks zrozumiałem jako indeks metematyczny (rzeczywisty) ciągu, a nie indeks python'a, więc indeks 1 to pierwszy wyraz ciągu
def fibonacci(starting_index, number_of_elements):  # m - starting index, n - number of elements
    n1 = 0
    n2 = 1

    for i in range(starting_index + number_of_elements - 1):  # - 1, cuz iterator is start from 0
        fibonacciElements.append(n1)
        n_sum = n1 + n2
        n1 = n2
        n2 = n_sum
        if i < starting_index - 1:  # remove elements which index is smaller than starting index
            del fibonacciElements[0]

    return fibonacciElements


if __name__ == '__main__':
    try:
        m = int(sys.argv[1])
        n = int(sys.argv[2])
        if m < 1 or n < 1:
            sys.stdout.write('m must be grater than 1 and n must be greater than 1')
            sys.exit(1)
        print('Input OK: Starting program')
    except ValueError:
        sys.stdout.write('Please enter a proper number/type of number')
        sys.exit(1)
    except IndexError:
        sys.stdout.write('Please enter proper amount of arguments')
        sys.exit(1)
sys.stdout.write('Fibonacci elements ({}) from index {}: {}'.format(n, m, (fibonacci(m, n)[:])))
