import sys
import os

if __name__ == '__main__':
    try:
        # codes that read words form both files
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text1 = os.path.join(dir_path, sys.argv[1])  # for problem with finding file only by name
        f1 = open(text1)
        words_in_text1 = []
        for line in f1:
            for word in line.split():
                words_in_text1.append(word)
        f1.close()

        dir_path = os.path.dirname(os.path.realpath(__file__))
        text2 = os.path.join(dir_path, sys.argv[2])  # for problem with finding file only by name
        f2 = open(text2)
        words_in_text2 = []
        for line in f2:
            for word in line.split():
                words_in_text2.append(word)
        f2.close()
        # end of reading files

        words_in_both_texts = []  # empty list of the same words form texts
        for word in words_in_text1:
            if word.casefold() in words_in_text2:  # ignore case and add word to list
                words_in_both_texts.append(word)

        if len(words_in_both_texts) == 0:
            print("Any word appear in both text files")  # if list is empty
        else:
            print("List of words that appear in both texts: {}".format(sorted(words_in_both_texts)))

    except FileNotFoundError:
        sys.stdout.write('File not exist')
    except IndexError:
        sys.stdout.write('Please enter proper amount of arguments')
        sys.exit(1)
