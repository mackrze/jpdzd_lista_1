import sys

if __name__ == '__main__':
    try:
        pesel = int(sys.argv[1])
        if len(str(pesel)) != 11:
            print('PESEL must have 11 digits')
            sys.exit(1)
        print('Input OK: Starting program')
        pesel_str = str(pesel)
        rr = int(pesel_str[0:2])
        mm = int(pesel_str[2:4])
        dd = int(pesel_str[4:6])
        gender_number = int(pesel_str[9:10])
        if rr < 0:
            sys.stdout.write('Wrong year declaration')
            sys.exit(1)

        if 32 < dd or dd < 1:
            sys.stdout.write('Wrong day declaration')
            sys.exit(1)

        if 0 < mm < 13:
            rr += 1900
        elif 20 < mm < 33:
            mm -= 20
            rr += 2000
        else:
            sys.stdout.write('Wrong month declaration')
            sys.exit(1)

        gender = 'K'
        if gender_number % 2 == 1:
            gender = 'M'
        sys.stdout.write('{}-{}-{}-{}'.format(dd, mm, rr, gender))
    except ValueError:
        sys.stdout.write('Please enter a proper number/type of number')
        sys.exit(1)
    except IndexError:
        sys.stdout.write('Please enter proper amount of arguments')
        sys.exit(1)
